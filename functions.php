<?php
/**
 * Child Theme functions loads the main theme class and extra options
 *
 * @package Omega Child
 * @subpackage Child
 * @since 1.3
 *
 * @copyright (c) 2013 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.0
 */

function oxy_load_child_scripts() {
    wp_enqueue_style( THEME_SHORT . '-child-theme' , get_stylesheet_directory_uri() . '/style.css', array( THEME_SHORT . '-theme' ), false, 'all' );
}
add_action( 'wp_enqueue_scripts', 'oxy_load_child_scripts');



/* 	============================================================
	Custom Code By RS
	============================================================ */

add_action( 'wp_footer', 'mycustom_wp_footer' );
 
function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '4991' == event.detail.contactFormId ) {
        window.location = "https://www.hanhaa.com/viewhanhaawebinars/";
    }
    }, false );
</script>
<?php
}

/* 	============================================================
	Custom Code By Stephen Hartnett
	============================================================ */

	//This removes the annoying Jetpack upsell message on the wordpress admin dashboard 

add_filter( 'jetpack_just_in_time_msgs', '_return_false' );

	/* 	============================================================
							WooCommerce 
	============================================================ */

	/* 	============================================================
							WooCommerce - cart changes  
	============================================================ */


// This removes product image from the cart
add_filter('woocommerce_cart_item_thumbnail','_return_false');

//This removes the link to the link to the product on the cart
function sv_remove_cart_product_link( $product_link, $cart_item, $cart_item_key ) {
    $product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    return $product->get_title();
}
add_filter( 'woocommerce_cart_item_name', 'sv_remove_cart_product_link', 10, 3 );

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );

remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
remove_action( 'woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20 );

remove_action( 'woocommerce_cart_actions', 'update_cart_action', 20 );

//This allows only one product in the cart at a time 
add_filter( 'woocommerce_add_to_cart_validation', 'bbloomer_only_one_in_cart', 99, 2 );
  
function bbloomer_only_one_in_cart( $passed, $added_product_id ) {
 
// empty cart first: new item will replace previous
wc_empty_cart();
return $passed;
}

//This redirects the customer to the checkout page 
add_filter('woocommerce_add_to_cart_redirect', 'themeprefix_add_to_cart_redirect');
function themeprefix_add_to_cart_redirect() {
 global $woocommerce;
 $checkout_url = wc_get_checkout_url();
 return $checkout_url;
}

//This changes the text of the add to the cart button to get started 
add_filter( 'woocommerce_product_single_add_to_cart_text', 'themeprefix_cart_button_text' );
add_filter(' woocomerce_product_add_to_cart_text', 'themeprefix_cart_button_text');
 
function themeprefix_cart_button_text() {
 return __( 'Proceed to checkout', 'woocommerce' );
}

//This removes the old text in the cart empty page 

remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );

//This adds the new text to the empty cart page

add_action('woocommerce_cart_is_empty', 'checkout_content_empty_cart');

function checkout_content_empty_cart() {
    echo "You have no items in the checkout, please return to the get started page and pick an option";
}

function wc_empty_cart_redirect_url() {
    return 'https://www.hanhaa.com/parcelive/get-started/';
}

//add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );

//This disables the shop page

//function woocommerce_disable_shop_page() {
 //   global $post;
 //   if (is_shop() || is_product() || is_product_category() || is_product_tag() ):
  //  global $wp_query;
 //   $wp_query->set_404();
//   status_header(404);
//    endif;
//}
//add_action( 'wp', 'woocommerce_disable_shop_page' );


// This removes the message added to your cart

add_filter( 'wc_add_to_cart_message_html', '__return_null' );

//This removes related post from single product page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

//This removes the category from the single product page 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );



//This removes the sidebar from the single product page
add_action( 'wp', 'bbloomer_remove_sidebar_product_pages' );
 
function bbloomer_remove_sidebar_product_pages() {
if ( is_product() ) {
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
  }
}

//This moves the stock value for single products to below the title 

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart',  10);


function wc_qty_add_product_field() {
	echo '<div class="options_group">';
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_wc_min_qty_product', 
			'label'       => __( 'Minimum Quantity', 'woocommerce-max-quantity' ), 
			'placeholder' => '',
			'desc_tip'    => 'true',
			'description' => __( 'Optional. Set a minimum quantity limit allowed per order. Enter a number, 1 or greater.', 'woocommerce-max-quantity' ) 
		)
	);
	echo '</div>';
	echo '<div class="options_group">';
	woocommerce_wp_text_input( 
		array( 
			'id'          => '_wc_max_qty_product', 
			'label'       => __( 'Maximum Quantity', 'woocommerce-max-quantity' ), 
			'placeholder' => '',
			'desc_tip'    => 'true',
			'description' => __( 'Optional. Set a maximum quantity limit allowed per order. Enter a number, 1 or greater.', 'woocommerce-max-quantity' ) 
		)
	);
	echo '</div>';	
}
add_action( 'woocommerce_product_options_inventory_product_data', 'wc_qty_add_product_field' );

/*
* This function will save the value set to Minimum Quantity and Maximum Quantity options
* into _wc_min_qty_product and _wc_max_qty_product meta keys respectively
*/
function wc_qty_save_product_field( $post_id ) {
	$val_min = trim( get_post_meta( $post_id, '_wc_min_qty_product', true ) );
	$new_min = sanitize_text_field( $_POST['_wc_min_qty_product'] );
	$val_max = trim( get_post_meta( $post_id, '_wc_max_qty_product', true ) );
	$new_max = sanitize_text_field( $_POST['_wc_max_qty_product'] );
	
	if ( $val_min != $new_min ) {
		update_post_meta( $post_id, '_wc_min_qty_product', $new_min );
	}
	if ( $val_max != $new_max ) {
		update_post_meta( $post_id, '_wc_max_qty_product', $new_max );
	}
}
add_action( 'woocommerce_process_product_meta', 'wc_qty_save_product_field' );

/*
* Setting minimum and maximum for quantity input args. 
*/
function wc_qty_input_args( $args, $product ) {
	
	$product_id = $product->get_parent_id() ? $product->get_parent_id() : $product->get_id();
	
	$product_min = wc_get_product_min_limit( $product_id );
	$product_max = wc_get_product_max_limit( $product_id );	
	if ( ! empty( $product_min ) ) {
		// min is empty
		if ( false !== $product_max ) {
			$args['min_value'] = $product_min;
		}
	}
	if ( ! empty( $product_max ) ) {
		// max is empty
		if ( false !== $product_max ) {
			$args['max_value'] = $product_max;
		}
	}
	if ( $product->managing_stock() && ! $product->backorders_allowed() ) {
		$stock = $product->get_stock_quantity();
		$args['max_value'] = min( $stock, $args['max_value'] );	
	}
	return $args;
}
add_filter( 'woocommerce_quantity_input_args', 'wc_qty_input_args', 10, 2 );
function wc_get_product_max_limit( $product_id ) {
	$qty = get_post_meta( $product_id, '_wc_max_qty_product', true );
	if ( empty( $qty ) ) {
		$limit = false;
	} else {
		$limit = (int) $qty;
	}
	return $limit;
}
function wc_get_product_min_limit( $product_id ) {
	$qty = get_post_meta( $product_id, '_wc_min_qty_product', true );
	if ( empty( $qty ) ) {
		$limit = false;
	} else {
		$limit = (int) $qty;
	}
	return $limit;
}
//This will sent the failed and cancelled email to the customer as well
add_action('woocommerce_order_status_changed', 'custom_send_email_notifications', 10, 4 );
function custom_send_email_notifications( $order_id, $old_status, $new_status, $order ){
    if ( $new_status == 'cancelled' || $new_status == 'failed' ){
        $wc_emails = WC()->mailer()->get_emails(); // Get all WC_emails objects instances
        $customer_email = $order->get_billing_email(); // The customer email
    }

    if ( $new_status == 'cancelled' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_Cancelled_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_Cancelled_Order']->trigger( $order_id );
    } 
    elseif ( $new_status == 'failed' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_failed_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_failed_Order']->trigger( $order_id );
    } 
}
//This adds a conformation field for the email address on the checkout page 
add_filter( 'woocommerce_checkout_fields' , 'bbloomer_add_email_verification_field_checkout' );
  
function bbloomer_add_email_verification_field_checkout( $fields ) {
 
$fields['billing']['billing_email']['class'] = array('form-row-first');
 
$fields['billing']['billing_em_ver'] = array(
    'label'     => __('Email Address Verification', 'bbloomer'),
    'required'  => true,
    'class'     => array('form-row-last'),
    'clear'     => true
);
 
return $fields;
}
 
// ---------------------------------
// 3) Generate error message if field values are different
 
add_action('woocommerce_checkout_process', 'bbloomer_matching_email_addresses');
 
function bbloomer_matching_email_addresses() { 
$email1 = $_POST['billing_email'];
$email2 = $_POST['billing_em_ver'];
if ( $email2 !== $email1 ) {
wc_add_notice( __( 'Your email addresses do not match', 'bbloomer' ), 'error' );
 }
}
/**
 * Auto Complete all WooCommerce virtual orders.
 * 
 * @param  int 	$order_id The order ID to check
 * @return void
 */
function custom_woocommerce_auto_complete_virtual_orders( $order_id ) {
	// if there is no order id, exit
	if ( ! $order_id ) {
		return;
	}
	// get the order and its exit
	$order = wc_get_order( $order_id );
	$items = $order->get_items();
	// if there are no items, exit
	if ( 0 >= count( $items ) ) {
		return;
	}
	// go through each item
	foreach ( $items as $item ) {
		// if it is a variation
		if ( '0' != $item['variation_id'] ) {
			// make a product based upon variation
			$product = new WC_Product( $item['variation_id'] );
		} else {
			// else make a product off of the product id
			$product = new WC_Product( $item['product_id'] );
		}
		// if the product isn't virtual, exit
		if ( ! $product->is_virtual() ) {
			return;
		}
	}
	/*
	 * If we made it this far, then all of our items are virual
	 * We set the order to completed.
	 */
	$order->update_status( 'completed' );
}
add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_virtual_orders' );

//This sends the woocommerce user to a custom login page prevents people fromn accessing the my-account 

add_action( 'template_redirect', 'wc_redirect_non_logged_to_login_access');
function wc_redirect_non_logged_to_login_access() {

    if (!is_user_logged_in() && is_account_page() ) {
       wp_redirect( home_url( '/customer-login' ) );
     exit();
   }
};

//This adds GDPR compliant section to the woocommerce registration page 
add_action( 'woocommerce_register_form', 'wpglorify_add_registration_privacy_policy', 11 );
   
function wpglorify_add_registration_privacy_policy() {
  
woocommerce_form_field( 'privacy_policy_reg', array(
    'type'          => 'checkbox',
    'class'         => array('form-row privacy'),
    'label_class'   => array('label'),
    'input_class'   => array('check'),
    'required'      => true,
    'label'         => 'I\'ve read and accept the Privacy Policy',
)); 
  
}
  
// Show error if the user does not tick
   
add_filter( 'woocommerce_registration_errors', 'wpglorify_validate_privacy_registration', 10, 3 );
  
function wpglorify_validate_privacy_registration( $errors, $username, $email ) {
    if ( ! (int) isset( $_POST['privacy_policy_reg'] ) ) {
        $errors->add( 'privacy_policy_reg_error', __( 'Privacy Policy consent is required!', 'woocommerce' ) );
    }
return $errors;
}

//This is the verification process for new woocommerce users 

function wc_registration_redirect( $redirect_to ) {     // prevents the user from logging in automatically after registering their account
    wp_logout();
    wp_redirect( '/verify/?n=');                        // redirects to a confirmation message
    exit;
}

function wp_authenticate_user( $userdata ) {            // when the user logs in, checks whether their email is verified
    $has_activation_status = get_user_meta($userdata->ID, 'is_activated', false);
    if ($has_activation_status) {                           // checks if this is an older account without activation status; skips the rest of the function if it is
        $isActivated = get_user_meta($userdata->ID, 'is_activated', true);
        if ( !$isActivated ) {
            my_user_register( $userdata->ID );              // resends the activation mail if the account is not activated
            $userdata = new WP_Error(
                'my_theme_confirmation_error',
                __( '<strong>Error:</strong> Your account has to be activated before you can login. Please click the link in the activation email that has been sent to you.<br /> If you do not receive the activation email within a few minutes, check your spam folder or <a href="/verify/?u='.$userdata->ID.'">click here to resend it</a>.' )
            );
        }
    }
    return $userdata;
}

function my_user_register($user_id) {               // when a user registers, sends them an email to verify their account
    $user_info = get_userdata($user_id);                                            // gets user data
    $code = md5(time());                                                            // creates md5 code to verify later
    $string = array('id'=>$user_id, 'code'=>$code);                                 // makes it into a code to send it to user via email
    update_user_meta($user_id, 'is_activated', 0);                                  // creates activation code and activation status in the database
    update_user_meta($user_id, 'activationcode', $code);
    $url = get_site_url(). '/verify/?p=' .base64_encode( serialize($string));       // creates the activation url
    $html = ( 'Please click <a href="'.$url.'">here</a> to verify your email address and complete the registration process.' ); // This is the html template for your email message body
    wc_mail($user_info->user_email, __( 'Activate your Account' ), $html);          // sends the email to the user
}

function my_init(){                                 // handles all this verification stuff
    if(isset($_GET['p'])){                                                  // If accessed via an authentification link
        $data = unserialize(base64_decode($_GET['p']));
        $code = get_user_meta($data['id'], 'activationcode', true);
        $isActivated = get_user_meta($data['id'], 'is_activated', true);    // checks if the account has already been activated. We're doing this to prevent someone from logging in with an outdated confirmation link
        if( $isActivated ) {                                                // generates an error message if the account was already active
            wc_add_notice( __( 'This account has already been activated. Please log in with your username and password.' ), 'error' );
        }
        else {
            if($code == $data['code']){                                     // checks whether the decoded code given is the same as the one in the data base
                update_user_meta($data['id'], 'is_activated', 1);           // updates the database upon successful activation
                $user_id = $data['id'];                                     // logs the user in
                $user = get_user_by( 'id', $user_id ); 
                if( $user ) {
                    wp_set_current_user( $user_id, $user->user_login );
                    wp_set_auth_cookie( $user_id );
                    do_action( 'wp_login', $user->user_login, $user );
                }
                wc_add_notice( __( '<strong>Success:</strong> Your account has been activated! You have been logged in and can now use the site to its full extent.' ), 'notice' );
            } else {
                wc_add_notice( __( '<strong>Error:</strong> Account activation failed. Please try again in a few minutes or <a href="/verify/?u='.$userdata->ID.'">resend the activation email</a>.<br />Please note that any activation links previously sent lose their validity as soon as a new activation email gets sent.<br />If the verification fails repeatedly, please contact our administrator.' ), 'error' );
            }
        }
    }
    if(isset($_GET['u'])){                                          // If resending confirmation mail
        my_user_register($_GET['u']);
        wc_add_notice( __( 'Your activation email has been resent. Please check your email and your spam folder.' ), 'notice' );
    }
    if(isset($_GET['n'])){                                          // If account has been freshly created
        wc_add_notice( __( 'Thank you for creating your account. You will need to confirm your email address in order to activate your account. An email containing the activation link has been sent to your email address. If the email does not arrive within a few minutes, check your spam folder.' ), 'notice' );
    }
}

// the hooks to make it all work
add_action( 'init', 'my_init' );
add_filter('woocommerce_registration_redirect', 'wc_registration_redirect');
add_filter('wp_authenticate_user', 'wp_authenticate_user',10,2);
add_action('user_register', 'my_user_register',10,2);

//This redirects the customer to their my account page after loggin in 

add_filter('woocommerce_login_redirect', 'wc_login_redirect');
 
function wc_login_redirect( $redirect_to ) {
     $redirect_to = 'https://www.hanhaa.com/my-account/';
     return $redirect_to;
}
/* 	============================================================
							WooCommerce - add product purchased to admin orders page 
	============================================================ */

add_filter('manage_edit-shop_order_columns', 'misha_order_items_column' );
function misha_order_items_column( $order_columns ) {
    $order_columns['order_products'] = "Purchased products";
    return $order_columns;
}
 
add_action( 'manage_shop_order_posts_custom_column' , 'misha_order_items_column_cnt' );
function misha_order_items_column_cnt( $colname ) {
	global $the_order; // the global order object
 
 	if( $colname == 'order_products' ) {
 
		// get items from the order global object
		$order_items = $the_order->get_items();
 
		if ( !is_wp_error( $order_items ) ) {
			foreach( $order_items as $order_item ) {
 
 				echo $order_item['quantity'] .'&nbsp;&times;&nbsp;<a href="' . admin_url('post.php?post=' . $order_item['product_id'] . '&action=edit' ) . '">'. $order_item['name'] .'</a><br />';
				// you can also use $order_item->variation_id parameter
				// by the way, $order_item['name'] will display variation name too
 
			}
		}
 
	}
 
}

/* 	============================================================
	WooCommerce - maintance mode for woocommerce pages 
	============================================================ */	
add_filter( 'woocommerce_general_settings', 'sh_woocommerce_maintenance_mode' );
function sh_woocommerce_maintenance_mode( $settings ) {

	$settings[] = array( 'name' => __( 'Custom Woocommerce Settings by Stephen Hartnett', 'textdomain' ), 'type' => 'title', 'desc' => '', 'id' => 'sh_woocommerce_maintenance_mode' );
					
	$settings[] = array(
		'title'    	=> __( 'Woocommerce Maintenance Mode', 'textdomain' ),
		'desc'     	=> __( 'When this option is enabled, checkout is disabled', 'textdomain' ),
		'id'       	=> 'woocommerce_sh_select_field',
		'css'      	=> 'min-width:350px;',
		'class'    	=> 'select',
		'default'	=> 'a',
		'type'     	=> 'select',
		'options'  	=> array(
			'a'	=> 'Disabled',
			'b'	=> 'Enabled',
		),
		'desc_tip' =>  true,
	);

	$settings[] = array( 'type' => 'sectionend', 'id' => 'sh_woocommerce_maintenance_mode');

	return $settings;
};
add_action( 'template_redirect', 'is_maintence_mode_enabled');
function is_maintence_mode_enabled() {
	$maintence_mode_enabled = get_option( 'woocommerce_sh_select_field'); 
		
	if($maintence_mode_enabled == "b" && !current_user_can('administrator') && is_checkout() ) { 
	wp_redirect( home_url( '/maintenance-mode') );
	exit();
 };
};